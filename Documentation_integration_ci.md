# Documentation projet CI/CD

Le but de ce projet pratique est de mettre en œuvre les compétences acquises durant les jours de cours. 
Il consiste à effectuer une intégration continue à partir d'un projet. Ce projet, dans mon cas, m'a été délivré par Monsieur Damien Duportal. Il s'agit d'un projet codé en Java utilisant l'outil de build Maven. Pour effectuer l'intégration continue de ce projet, j'ai choisi d'utiliser l'outil GitLab-ci. 
Ci-dessous, est décrit l'ensemble des étapes de mon pipeline.

## Objectifs du projet 

- Utilisation d'un outil d'intégration continue différent de Jenkins.
- Effectuer une intégration continue avec un pipeline 
- Explication du pipeline 


## Before script

L’outil GitLab utilise des machines virtuelles (gitlab runner) pour mettre en œuvre le pipeline (compilation, test, déploiement et mise en place sur site statique). Il m’a donc fallu créer au préalable un token permettant de ramener mon site statique sur mon dépôt local. 

## Compilation

- compilation du code source

L’outil GitLab utilise des machines virtuelles (gitlab runner) pour mettre en œuvre le pipeline (compilation, test, déploiement et mise en place sur site statique). Il m’a donc fallu créer au préalable un token permettant de ramener mon site statique sur mon dépôt local. 

- génération de la documentation 

En parallèle, un travail de génération du site statique est lancé. La commande « mvn site » est lancée. Elle permet de générer le site web statique de documentation. Cependant cette documentation est dans un premier temps stocké sur la machine virtuelle. Il a donc fallu ramener le résultat de maven site sur mon dépôt local de GitLab. 

## Test 

Pour ce travail, la commande « mvn test » a été utilisée. Elle permet d’exécuter l’ensemble des tests contenus dans le projet. Deux jobs de test sont lancés simultanément dans cette phase. Ils sont effectués avec deux versions de Java différentes (Java 8 et 11). Si l’un des deux tests échoue alors le pipeline s’interrompt.


## Déploiement 

- déploiment du projet 

 Une fois que les tests sont réussis, la phase de déploiement peut commencer. Elle s’exécute à l’aide de la commande « mvn deploy ». Elle permet déployer l’ensemble du projet. 

- déploiement du site de documentation 

Cette étape évite de publier de la documentation non officielle sur le site Web mais peut toujours consulter la documentation en tant que site Web statique. Il est déployé à l'ai de de GitLab Page. 
Dans mon cas, un bug GitLab est intervenu. Le travail de génération de la GitLab Page s’est bien déroulé et les artéfacts se sont bien téléchargé cependant la page n’a pas pu être générer via une URL. 
J’ai alors effectué des recherches pour comprendre la source du problème mais il s’emblerait que ce soit un bug interne de GitLab. (URL : https://gitlab.com/gitlab-org/gitlab/issues/1719)

![](Capture_ecran_gitlab_page.png)

![](Capture_ecran_pipeline.png)